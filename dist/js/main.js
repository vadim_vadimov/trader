$(function () {

    jQuery.extend(jQuery.validator.messages, {
        email: "",
    });


  $('.quote-form-js').each(function(i, item) {
       var $form = $(item);

       $form.validate({
           // onclick: function(element) {
           //     if ( element.name in this.submitted ) {
           //         this.element( element );
           //     } else if ( element.parentNode.name in this.submitted ) {
           //         this.element( element.parentNode );
           //     }
           //     setTimeout(function () {
           //         $(".quote__field-item").each(function(i, item) {
           //             if ($(item).find("label.error").is(':visible')) {
           //                 $(item).find(".quote__field-inputs").addClass("borders");
           //             } else {
           //                 $(item).find(".quote__field-inputs").removeClass("borders");
           //             }
           //         });
           //     }, 50);
           // },
           // invalidHandler: function(form, validator) {
           //     setTimeout(function () {
           //         $(".quote__field-item").each(function(i, item) {
           //             if ($(item).find("label").hasClass("error")) {
           //                 $(item).find(".quote__field-inputs").addClass("borders");
           //             } else {
           //                 $(item).find(".quote__field-inputs").removeClass("borders");
           //             }
           //         });
           //     }, 50);
           //  },
            rules: {
             marketplace: {
                 required: true,
             },
             age: {
                 required: true,
             },
             orders: {
                 required: true,
             },
             feedback: {
                 required: true,
             },
             sold: {
                 required: true,
             },
             FBA: {
                 required: true,
             },
             email: {
                 required: true,
             },

            },
            messages: {
             marketplace: {
                 required: ''
             },
             age: {
                 required: ''
             },
             orders: {
                 required: ''
             },
             feedback: {
                 required: ''
             },
             sold: {
                 required: ''
             },
             FBA: {
                 required: ''
             },
             email: {
                 required: ''
             },
            },
            submitHandler: function(form) {
                var $form = $(form);
                $.ajax({
                    type: "POST",
                    url: "quote-field.php",
                    data: $form.serialize()
                }).done(function(e) {
                    $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                    $form.trigger('reset');
                    $.fancybox.open({
                        src:'#popup-thanks'
                    });
                }).fail(function (e) {
                    $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                    $form.trigger('reset');
                    setTimeout(function () {
                        $form.find('.dispatch-message').slideUp();
                    }, 5000);
                })
            }
       });

    });

  $('.help-form-js').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             name: {
                 required: true,
             },
             email: {
                 required: true,
             },
             msg: {
                 required: true,
             },

         },
         messages: {
             name: {
                 required: ''
             },
             email: {
                 required: ''
             },
             msg: {
                 required: ''
             },
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "help-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 $.fancybox.open({
                     src:'#popup-thanks'
                 });
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

    });

    $('.scroll').on('click', function(e) {
        e.preventDefault();

        var toBlock = $(this).attr("href");
        var scrolling = $(toBlock).offset().top;

        $('html, body').animate({
            scrollTop: scrolling - 90
        }, 1100);
    });



    $( ".help__list" ).each(function() {
        $(".help__item-btn").on('click', function () {
            $(this).toggleClass("open");
            $(this).next(".help__item-tab").slideToggle();
        });
    });

    $('.burger-content').on('click', function () {
      $('.header__menu').addClass('active');
      $('body').addClass('overflowHidden');
    });

    $('.mobile-menu-close').on('click', function () {
      $('.header__menu').removeClass('active');
      $('body').removeClass('overflowHidden');
    });




    $('.header__menu-list a').on('click', function () {
      $('.header__menu').removeClass('active');
      $('body').removeClass('overflowHidden');
    });

    $(window).scroll(function(){

                var sticky = $('.fix-header'),
                    scroll = $(window).scrollTop();
                    y = $(this).scrollTop();

                if (scroll >= $('.fix-start').height() + 100) sticky.addClass('fixed');
                else sticky.removeClass('fixed');

                // $('.header__menu-list a').each(function (event) {
                //     id = $(this).attr('href');
                //         if (y >= $(id).offset().top - 100) {
                //             $('.header__menu-list a').not(this).removeClass('active');
                //             $(this).addClass('active');   
                //     }
                // });


                if ($('.fix-header').hasClass('fixed')) {
                    $('.intro-block').css('padding-top', '97px');
                }   else {
                    $('.intro-block').css('padding-top', '0');
                }
    });




});